var Class = require('plus.class');

var Master = new Class({

    init: function (config, slaves, dir, socketizer, JSONStreamer, printer) {
        this.config = config;

        this.slaves = slaves;
        this.socketizer = socketizer;
        this.dir = dir;
        this.JSONStreamer = JSONStreamer;
        this.printer = printer;

    },

    registerSlave: function (info, next) {

        var api = this.socketizer.provide(require('../slave/Slave'), this.socket);

        var Slave = require('./Slave');

        var slave = new Slave(api, this.printer);
        slave.ip = info.ip;

        this.slaves.add(slave);

        console.log('Registered slave', '#' + slave.id, slave.ip);

        next(null, {id: slave.id});

    },
    upload: function (next) {
        this.slaves.upload(this.dir, next);
    },
    run: function (command, next) {
        this.slaves.run(command, next);
    },
    job: function (command, next) {
        this.slaves.job(command, next);
    },
    jobs: function (jobsStream, next) {
        this.slaves.jobs(jobsStream, next);
    },
    getResultQueue: function (next) {
        next(null, this.slaves.resultStream.pipe(this.JSONStreamer.stringify()));
    },
    disconnectBySocket: function (socket) {

        var slave = this.slaves.getBySocket(socket);

        if (slave) {
            this.slaves.removeBySocket(socket);
            console.log('Unregistered slave', '#' + slave.id);
        }
    },
    stopAll: function (next) {

        this.slaves.stop(function () {

            next();

            setTimeout(function () {
                console.log('master stopped');
                process.exit(0);
            }, 100);

        });
    },
    waitSlaves: function (quantity, next) {
        this.slaves.waitSlaves(quantity, next)
    }




});

module.exports = Master;


