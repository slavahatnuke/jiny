var Class = require('plus.class');

var MasterServer = new Class({

    init: function (config, slaves, socketizer, JSONStreamer, printer) {

        this.config = config;
        this.socketizer = socketizer;
        this.slaves = slaves;
        this.JSONStreamer = JSONStreamer;
        this.printer = printer;

        this.server = null;
        this.io = null;


        this.host = this.config.get('host');
        this.port = this.config.get('port');
        this.dir = this.config.get('dir');

    },

    start: function () {

        var self = this;

        this.server = require('http').createServer(function (req, res) {
            res.setHeader("Content-Type", "text/plain");
            res.writeHead(200);

            console.log('HTTP health:', 'OK');
            res.end('OK\n');
        });

        this.io = require('socket.io')(this.server);
        this.server.listen(self.config.get('port'));

        console.log('I am master:', 'http://' + self.host + ':' + self.port);
        console.log('Host:', self.host);
        console.log('Port:', self.port);
        console.log('Dir:', self.dir);

        var Master = require('./Master');

        this.io.on('connection', function (socket) {

            var master = new Master(self.config, self.slaves, self.dir, self.socketizer, self.JSONStreamer, self.printer);
            self.socketizer.connect(master, socket);

            socket.on('disconnect', function () {
                master.disconnectBySocket(socket);
            });

        });


        if (!this.config.get('live')) {
            this.bindOnExit();
        }

    },

    bindOnExit: function () {

        var stop = function () {
            this.slaves.stop(function () {

                setTimeout(function () {
                    console.log('master stopped');
                    process.exit(0);
                }, 100);

            });
        }.bind(this);

        process.on('SIGINT', stop);
        process.on('SIGTERM', stop);
        process.on('exit', stop);
    }

});

module.exports = MasterServer;


