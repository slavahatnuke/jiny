var Class = require('plus.class');

var Slave = new Class({

    init: function (api, printer) {
        this.api = api;

        this.printer = printer;

        this.busy = false;
        this.id = null;
        this.ip = null;
    },

    upload: function (fileStream, next) {

        var self = this;

        this.api.upload(fileStream, function (error, info) {
            console.log('#' + info.slave, info.ip, 'uploaded', 'dir:', info.dir, 'time:', ''+ info.time + 's');
            next(error, info);
        });
    },

    command: function (command, next) {

        var self = this;

        this.api.command(command, function (error, info) {
            self.printer.printJobInfo(info);
            next(error, info);
        });
    },
    stop: function (next) {
        this.api.stop(function (error) {
            console.log('#' + this.id, 'stopped');
            next(error);
        }.bind(this));
    }

});

module.exports = Slave;


