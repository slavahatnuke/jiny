var Class = require('plus.class');

var SlaveServer = new Class({

    init: function (config, socketizer, printer) {

        this.config = config;
        this.socketizer = socketizer;
        this.printer = printer;

        this.socket = null;

        this.host = this.config.get('host');
        this.port = this.config.get('port');
        this.dir = this.config.get('dir');

        this.id = null;

    },

    start: function () {

        var self = this;

        var url = 'http://' + this.host + ':' + this.port;
        this.socket = require('socket.io-client')(url);

        console.log('I am slave', this.host);
        console.log('Socket:', url);

        console.log('Host:', this.host);
        console.log('Port:', this.port);
        console.log('Dir:', this.dir);

        var Slave = require('./Slave');
        var slave = new Slave(self.config, self.printer);

        this.master = this.socketizer.provide(require('../master/Master'), this.socket);
        this.slave = this.socketizer.connect(slave, this.socket);

        this.socket.on('connect', function () {

            this.master.registerSlave({ip: slave.ip}, function (err, info) {
                slave.id = info.id;
                console.log('Registered as:', '#' + slave.id);
            });

        }.bind(this));


    }
});

module.exports = SlaveServer;


