var Class = require('plus.class');

var Slave = new Class({

    init: function (config, printer) {
        this.config = config;
        this.dir = this.config.get('dir');
        this.skipUpload = this.config.get('skipUpload');

        this.printer = printer;

        this.ip = require('ipv4');
        this.id = null;
    },

    upload: function (stream, next) {
        var self = this;

        if(self.skipUpload){

            stream.pipe(require('async-queue-stream')(), {end: false});

            stream.on('end', function () {
                console.log(self.dir, 'uploading skipped');
                next(null, self.getSlaveInfo());
            });

            return
        };

        console.log(self.dir, 'uploading... ');

        var startedAt = new Date();

        var spawn = require('child_process').spawn;

        var extract = spawn('tar', ['-C', self.dir, '-xzf', '-']);

        stream.pipe(extract.stdin, {end: false});

        extract.stdin.on('error', function () {
            console.log('error');
        });

        stream.on("end", function () {

            extract.stdin.end(function () {

                var time = new Date().getTime() - startedAt.getTime();
                var time = parseInt(time / 1000);

                console.log(self.dir, 'uploaded', '' + time + 's');

                var info = self.getSlaveInfo();
                info.time = time;

                next(null, info);

            });

        });
    },

    getSlaveInfo: function () {

        var info = {
            dir: this.dir,
            time: 0,
            slave: this.id,
            error: null,
            ip: this.ip
        };

        return info;
    },

    command: function (command, next) {

        var self = this;

        var exec = require('child_process').exec;

        var child = exec(command, {cwd: self.dir}, function (error, stdout, stderr) {

            var info = self.getSlaveInfo();
            info.stdout = stdout;
            info.stderr = stderr;
            info.command = command;
            info.error = error;

            self.printer.printJobInfo(info);

            next(error, info);
        });


    },
    stop: function (next) {
        console.log('#' + this.id, 'stopped');
        next(null);

        setTimeout(function () {
            process.exit(0);
        }, 100);

    }

});

module.exports = Slave;


