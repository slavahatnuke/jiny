var Class = require('plus.class');

var Timer = new Class({

    init: function () {
         this.start();
    },
    start: function () {
        this.startedAt = new Date();
    },
    getTime: function () {
        return new Date().getTime() - this.startedAt.getTime();
    },

    toString: function () {
        return '' + parseInt(this.getTime()/1000) + 's';
    }

});

module.exports = Timer;


