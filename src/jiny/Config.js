var Class = require('plus.class');

var Config = new Class({

    init: function (commander) {
        this.commander = commander;
    },
    get: function (name) {
        return this.commander[name] !== undefined ? this.commander[name] : undefined;
    },
    set: function (name, value) {
        return this.commander[name] = value;
    }
});

module.exports = Config;


