var Commander = function (info, jiny) {
    var commander = require('commander');
    require('../routes')(commander, info, jiny);
    return commander;
};

module.exports = Commander;

