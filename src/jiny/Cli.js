var Class = require('plus.class');

var Cli = new Class({

    init: function (comander) {
        this.comander = comander;
    },
    run: function () {

        this.comander.parse(process.argv);

        if (!this.comander.args.length)
            this.comander.outputHelp();
    }
});

module.exports = Cli;


