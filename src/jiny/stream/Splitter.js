var Splitter = function () {

    var BufferStream = require('bufferstream');

    var Splitter = function (handler) {

        var split = new BufferStream({encoding: 'utf8', size: 'flexible'});
        split.split("\n");

        handler = handler || function (line) {
            this.emit('data', line.toString());
        };

        split.on('split', handler);

        return split;
    };

    Splitter.stdin = function (handler) {
        return process.stdin.pipe(Splitter(handler));
    }

    return  Splitter

};
module.exports = Splitter;


