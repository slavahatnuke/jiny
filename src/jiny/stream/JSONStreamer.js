var JSONStreamerer = function () {

    var queueStream = require('async-queue-stream');

    return {

        parse: function () {
            return queueStream(function (data, next) {
                next(null, JSON.parse(data));
            });
        },

        stringify: function () {

            return queueStream(function (data, next) {
                next(null, JSON.stringify(data));
            });
        }

    }

};
module.exports = JSONStreamerer;


