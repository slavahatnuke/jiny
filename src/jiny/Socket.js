var Class = require('plus.class');

//TODO bind
var Socket = new Class({

    init: function (config) {

        this.config = config;

        this.host = this.config.get('host');
        this.port = this.config.get('port');

        this.url = 'http://' + this.host + ':' + this.port;
        this.socket = require('socket.io-client')(this.url);

    },

    getSocket: function () {
        return this.socket;
    },

    getHost: function () {
        return this.host;
    },

    getPort: function () {
        return this.port;
    },

    getUrl: function () {
        return this.url;
    }
});

module.exports = Socket;


