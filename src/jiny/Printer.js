var Class = require('plus.class');

var Printer = new Class({


    printJobInfo: function (info) {

        console.log('#' + info.slave, info.ip, '>', info.command);

        if (info.stderr)
            console.log(info.stderr);

        if (info.stdout)
            console.log(info.stdout);

    }

});

module.exports = Printer;


