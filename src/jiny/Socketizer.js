var Class = require('plus.class');

var Socketizer = new Class({

    init: function () {
        this.underscore = require('underscore');
        this.ss = require('socket.io-stream');
    },

    isStream: function (data) {
        return data instanceof require('stream').Stream;
    },

    connectMethod: function (object, name, socket) {

        var self = this;

        var method = object[name];

        if (self.underscore.isFunction(method)) {

            var sSocket = this.ss(socket);

            sSocket.on(name, function () {

                var args = self.underscore.toArray(arguments);

                args.push(function () {

                    var emitArgs = self.underscore.toArray(arguments);
                    emitArgs.unshift(name + '.done');

                    sSocket.emit.apply(sSocket, self.prepareArguments(emitArgs));
                });

                method.apply(object, args);
            });
        }

    },

    connect: function (object, socket) {

        object.socket = socket;

        for (var name in object)
            this.connectMethod(object, name, object.socket);
    },

    provide: function (aClass, socket) {

        var xClass = Class(aClass, {init: new Function()});

        var object = new xClass();
        object.socket = socket;

        for (var name in object)
            this.provideMethod(object, name, object.socket);

        return object;

    },

    provideMethod: function (object, name, socket) {

        var self = this;

        if (self.underscore.isFunction(object[name])) {

            object[name] = function () {

                var args = self.underscore.toArray(arguments);
                var callback = self.underscore.last(args);

                if (!self.underscore.isFunction(callback))
                    args.push(new Function());

                var callback = args.pop();

                args.unshift(name);

                var sSocket = self.ss(socket);

                sSocket.emit.apply(sSocket, self.prepareArguments(args));

                sSocket.once(name + '.done', function () {
                    callback.apply(object, arguments);
                });
            }
        }

    },
    prepareArguments: function (args) {

        var self = this;

        args.forEach(function (x, idx) {

            if (self.isStream(x)) {

                var stream = self.ss.createStream();
                args[idx] = stream;

                x.pipe(stream);

//@TODO unpipe
//
//                x.on('end', function () {
//
//                    if (self.underscore.isFunction(x.unpipe))
//                        x.unpipe(stream);
//
//                });

            }
        });

        return args;
    }

});

module.exports = Socketizer;


