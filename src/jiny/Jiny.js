var Class = require('plus.class');

var Jiny = new Class({

    init: function (container) {
        this.container = container;
    },
    cli: function () {
        this.container.get('Cli').run();
    },
    getContainer: function () {
        return this.container;
    },
    startMaster: function () {
        this.container.get('Detacher').handle(function () {
            this.container.get('MasterServer').start();
        }.bind(this));
    },
    startSlave: function () {
        this.container.get('Detacher').handle(function () {
            this.container.get('SlaveServer').start();
        }.bind(this));
    },
    upload: function () {
        this.container.get('MasterClient').upload();
    },
    run: function (command) {
        this.container.get('MasterClient').run(command);
    },
    jobs: function () {
        var client = this.container.get('MasterClient');
        client.jobs(this.container.get('Splitter').stdin());
    },
    stop: function () {

        this.container.get('MasterClient').stopAll();
    },
    waitSlaves: function (quantity) {
        this.container.get('MasterClient').waitSlaves(quantity);
    },
    feed: function (mask, pattern) {

        var dir = this.container.get('Config').get('dir');

        var pattern = pattern || "echo @";
        if (pattern.indexOf('@') == -1) pattern += ' @'

        var readdirp = require('readdirp');
        var es = require('event-stream');

        var jobStream = readdirp({ root: dir, fileFilter: mask })
            .pipe(es.mapSync(function (entry) {
                return pattern.replace('@', entry.path);
            }));

        this.container.get('MasterClient').jobs(jobStream);

    }
});

module.exports = Jiny;


