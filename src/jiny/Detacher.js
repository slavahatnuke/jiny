var Class = require('plus.class');

var Detacher = new Class({

    init: function (commander, config) {
        this.commander = commander;
        this.config = config;
    },

    handle: function (fn) {

        if (this.config.get('detach')) {

            var args = this.commander.rawArgs;

            var idx = args.indexOf('--detach');

            if (idx >= 0) {
                args.splice(idx, 1);
            }

            var cmd = args.shift();

            var spawn = require('child_process').spawn;

            var child = spawn(cmd, args, {
                detached: true,
                stdio: 'ignore'
            });

            child.on('exit', function (code) {
                console.log('EXITed', 'code:', code, 'command:', cmd, args.join(' '));
            });

            setTimeout(function () {
                console.log('done', cmd, args.join(' '));
                process.exit(0);
            }, 500);
        }
        else {
            fn();
        }
    }
});

module.exports = Detacher;


