module.exports = function (commander, info, jiny) {

    commander
        .version(info.version)
        .option('--port [port]', 'master port', parseFloat, 7500)
        .option('--host [host]', 'master host', String, require('ipv4'))
        .option('--dir [dir]', 'work dir', String, process.cwd())
        .option('--detach', 'runs detached process: master / slave')
        .option('--live', 'master option to save alive slaves after exit')
        .option('--skip-upload', 'slave option to skip upload on this slave, if you want to use slave on the master');

    commander
        .command('master')
        .description('Starts master')
        .action(function () {
            jiny.startMaster();
        });

    commander
        .command('slave')
        .description('Starts slave')
        .action(function () {
            jiny.startSlave();
        });

    commander
        .command('upload')
        .description('Upload dir to the slaves')
        .action(function () {
            jiny.upload();
        });

    commander
        .command('run [command]')
        .description('Runs command on the all slaves. Prepare project on the all slaves. `jiny run "./prepare.sh"`')
        .action(function (command) {
            jiny.run(command);
        });

    commander
        .command('feed [mask] [pattern]')
        .description('Find files by mask, build job based on pattern and runs jobs in parallel on different slaves. Replaces `@` -> `[file name]`. Example: `jiny --dir tests feed *.js "mocha -R spec @"`')
        .action(function (mask, pattern) {
            jiny.feed(mask, pattern);
        });

    commander
        .command('jobs')
        .description('Runs commands in  in parallel on the different slaves. Uses pipe: `find tests/ | grep .js | awk \'{print "echo "$1" && sleep 1"}\' | jiny jobs`')
        .action(function () {
            jiny.jobs();
        });

    commander
        .command('stop')
        .description('Stops slaves and master')
        .action(function () {
            jiny.stop();
        });

    commander
        .command('wait.slaves [quantity]')
        .description('Waits for slaves registered in the master')
        .action(function (quantity) {
            jiny.waitSlaves(quantity);
        });

}