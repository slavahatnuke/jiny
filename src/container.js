var Container = require('plus.container');
var container = new Container();



container.register('package.info', require('../package.json'));
container.register('Jiny', require('./jiny/Jiny'), ['container']);
container.register('Commander', require('./jiny/Commander'), ['package.info', 'Jiny']);

container.register('Cli', require('./jiny/Cli'), ['Commander']);
container.register('Config', require('./jiny/Config'), ['Commander']);

container.register('Socket', require('./jiny/Socket'), ['Config']);
container.register('Splitter', require('./jiny/stream/Splitter'), []);
container.register('JSONStreamer', require('./jiny/stream/JSONStreamer'), []);
container.register('Printer', require('./jiny/Printer'), []);
container.register('Detacher', require('./jiny/Detacher'), ['Commander', 'Config']);

container.register('MasterServer', require('./master/MasterServer'), ['Config', 'MasterSlaves', 'Socketizer', 'JSONStreamer', 'Printer']);
container.register('MasterClient', require('./client/MasterClient'), ['Config', 'Socketizer', 'JSONStreamer', 'Printer']);

container.register('MasterSlaves', require('./master/Slaves'), []);

container.register('SlaveServer', require('./slave/SlaveServer'), ['Config', 'Socketizer', 'Printer']);
container.register('Socketizer', require('./jiny/Socketizer'), []);

module.exports = container;
