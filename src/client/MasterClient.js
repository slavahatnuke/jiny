var Class = require('plus.class');

var MasterClient = new Class({

    init: function (config, socketizer, JSONStreamer, printer) {

        this.config = config;
        this.socketizer = socketizer;
        this.JSONStreamer = JSONStreamer;
        this.printer = printer;

        this.Timer = require('../jiny/Timer');

        this.socket = null;

        this.host = this.config.get('host');
        this.port = this.config.get('port');

        this.url = 'http://' + this.host + ':' + this.port;
        this.socket = require('socket.io-client')(this.url);

        console.log('I am client', this.url);

        this.master = this.socketizer.provide(require('../master/Master'), this.socket);
    },

    upload: function () {

        var timer = new this.Timer();
        this.master.upload(function (error, result) {

            result.forEach(function (info) {
                console.log('#' + info.slave, info.ip, 'uploaded', 'dir:', info.dir, 'time:', '' + info.time + 's');
            });

            //@TODO exit code
            console.log('done', timer.toString());
            process.exit(0);

        }.bind(this));

    },

    run: function (command) {

        var hasError = false;
        var self = this;

        var timer = new this.Timer();
        this.master.run(command, function (error, result) {

            result.forEach(function (info) {

                if(info.error)
                    hasError = true;

                self.printer.printJobInfo(info);
            });

            console.log(hasError ? 'FAILed' : 'PASSed', timer.toString());
            process.exit(hasError ? 255 : 0);
        });
    },

    job: function (command) {

        this.master.job(command, function (error, command) {
            console.log('[added]', command);
            process.exit(0);
        });
    },

    jobs: function (jobStream) {

        var hasError = false;
        var self = this;

        var timer = new this.Timer();

        this.master.getResultQueue(function (err, result) {

            var qs = require('async-queue-stream');

            result
                .pipe(self.JSONStreamer.parse())
                .pipe(qs(function (info, next) {

                    if(info.error)
                        hasError = true;

                    self.printer.printJobInfo(info);
                    next(null, info);
                }));
        });


        self.master.jobs(jobStream, function () {

            console.log(hasError ? 'FAILed' : 'PASSed', timer.toString());
            process.exit(hasError ? 255 : 0);

        });

    },
    stopAll: function () {
        this.master.stopAll(function () {
            console.log('done');
            process.exit(0);
        });
    },
    waitSlaves: function (quantity) {
        this.master.waitSlaves(quantity, function () {
            console.log('done');
            process.exit(0);
        });
    }


});

module.exports = MasterClient;


